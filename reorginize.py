import os
import shutil

# Define your desktop path
desktop_path = os.path.join(os.path.expanduser("~"), "Desktop")

# Define the folders for each file type
folders = {
    "Text Files": [".txt", ".md", ".rtf"],
    "PDFs": [".pdf"],
    "Images": [".jpg", ".jpeg", ".png", ".gif", ".bmp", ".tiff", ".svg", ".heic"],
    "Screenshots": [".png", ".jpg", ".jpeg"],  # Common screenshot formats
    "Word Documents": [".doc", ".docx"],
    "Excel Files": [".xls", ".xlsx", ".csv"],
    "PowerPoint Files": [".ppt", ".pptx"],
    "Archives": [".zip", ".rar", ".tar", ".gz", ".7z", ".bz2"],
    "Python Scripts": [".py"],
    "Audio Files": [".mp3", ".wav", ".aac", ".flac", ".m4a"],
    "Video Files": [".mp4", ".mov", ".avi", ".mkv", ".flv", ".wmv"],
    "HTML Files": [".html", ".htm"],
    "CSS Files": [".css"],
    "JavaScript Files": [".js"],
    "JSON Files": [".json"],
    "XML Files": [".xml"],
    "Executables": [".exe", ".bat", ".sh"],
    "Installers": [".dmg", ".pkg", ".app"],
    "Fonts": [".ttf", ".otf", ".woff", ".woff2"],
    "Others": []  # Any other files
}

# Create folders if they do not exist
for folder in folders:
    folder_path = os.path.join(desktop_path, folder)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

# Move files to the appropriate folders
for filename in os.listdir(desktop_path):
    file_path = os.path.join(desktop_path, filename)

    # Skip directories
    if os.path.isdir(file_path):
        continue

    # Determine file extension
    file_ext = os.path.splitext(filename)[1].lower()

    # Move the file to the correct folder
    moved = False
    for folder, extensions in folders.items():
        if file_ext in extensions:
            shutil.move(file_path, os.path.join(
                desktop_path, folder, filename))
            moved = True
            break

    # If no matching extension was found, move to "Others"
    if not moved:
        shutil.move(file_path, os.path.join(desktop_path, "Others", filename))

print("Files have been reorganized.")
